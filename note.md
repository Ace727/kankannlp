# EXOz专题自动生成
## DSL查询语句
'''
val query = {
      bool(
        must (
          termQuery("target", "instagram"),
          //geoDistanceQuery("geo").point(lationLat,lationLon).distance(5000,DistanceUnit.METERS),
          should(
            rangeQuery("timestamp").gte(date1start.getTime).lte(date1end.getTime),
              rangeQuery("timestamp").gte(date2start.getTime).lte(date2end.getTime)

          ),
          should(
            termQuery("content", "exordiuminhk"),
            termQuery("content", "4everchanbaek"),
            termQuery("content", "exo")
          ) minimumShouldMatch 1,
          rangeQuery("likesCount").gte(1)
        )
      )
'''
不加地理位置查询时有929条记录。
不加入exo关键字有169条数据。
加入经纬度过滤后有4条记录,测试了离中心点500-5000米,结果一致,说明post里包含地理位置较少。

主要考虑likescount和commentcount数据
其中likesCount大于1000的post有30,
commentCount大于50的post有8
commentCount大于20的post有32
likesCount 大于 50 和 commentCount 大于 5 的post有131

测试发现

查看postAPI: GET  http://prod-api.kankanapp.com.cn/api/v3/post?profileId=1450791591:instagram&timestamp=1486897639000