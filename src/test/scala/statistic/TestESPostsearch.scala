package statistic

import java.text.SimpleDateFormat
import java.util.Date

import org.junit.Test

import scala.collection.mutable.ArrayBuffer


/**
  * Created by remarkmedia on 17/2/27.
  */
class TestESPostsearch {
  @Test def testPostBot(): Unit ={
    val target = "instagram"
    val dateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-M-d H:m:s")
    val date1start : Date = dateFormat.parse("2017-2-11 19:00:00")
    val date1end : Date = dateFormat.parse("2017-2-12 22:00:00")
    val shouldKeyWords = List("exordiuminhk","4everchanbaek","EXOrDIUMinHongKong")
    val shouldkwords = Option(shouldKeyWords)
    val start = Option(date1start)
    val end = Option(date1end)
    val likescount = 5
    val commentscount = 0
    val mustKeyWords = None
    val targetNames = None
    val result = ESPostsearch.searchPostBot(target,start,end,shouldkwords,mustKeyWords,targetNames,likescount,commentscount)
    val hitsIter = result.iterator
    while(hitsIter.hasNext) {
      val hit = hitsIter.next()
      val post_id = hit.id()
      val sourceMap = hit.getSource
      //获取profileInfo
      val nickname = sourceMap.get("targetName").toString
      val avatar = sourceMap.get("targetAvatar").toString
      val targetId = sourceMap.get("targetId").toString
      val target = sourceMap.get("target").toString
      val postType = sourceMap.get("type").toString
      if (postType.equals("image") && likescount > 20) {
        val imageurl = HbasePostSearch.searchImageUrl(targetId, target, post_id)
        println(targetId+"\t"+postType+"\t"+imageurl)
      } else if (postType.equals("video")) {
        //postType 等于video 获取videoDetailInfo
        val videoDetailInfo = HbasePostSearch.searchVideoUrl(targetId, target, post_id)
        if (!videoDetailInfo.videoUrl.equals("") && !videoDetailInfo.coverUrl.equals("")) {
          println(targetId+"\t"+postType+"\t"+videoDetailInfo.videoUrl)
        }
      }
    }
  }

  def test(): Unit ={

  }
}
