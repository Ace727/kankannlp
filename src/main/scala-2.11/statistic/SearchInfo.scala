package statistic

import java.util.Date

/**
  * Created by remarkmedia on 17/3/9.
  */
class SearchInfo {
  var subCategoryId: Long = 0l
  var target: String = ""
  var startTime: Option[Date] = None
  var endTime: Option[Date] = None
  var shouldKeyWords: Option[List[String]] = None
  var mustKeyWords: Option[List[String]] = None
  var targetNames: Option[List[String]] = None
  var likesCount: Int = 0
  var commentsCount: Int = 0
  var imageLevel:Int = 0
  var videoLevel:Int = 0

}
