package statistic

abstract class ServiceMsg

case class BatchTrendingDetailMessage(data: List[TrendingDetailInfo]) extends ServiceMsg

case class TrendingDetailInfo(subCategoryId: Long, postId: String, timestamp: Long, profile: ProfileInfo, postType: String, content: String,
                              imageUrl: Option[String], video: Option[VideoDetailInfo], likeCount: Int, commentCount: Int)

case class ProfileInfo(nickname: String, avatar: String, targetId: String, platform: String)

case class VideoDetailInfo(coverUrl: String, videoUrl: String)