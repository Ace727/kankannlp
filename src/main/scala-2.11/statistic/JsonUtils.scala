package statistic

import org.json4s.FieldSerializer._
import org.json4s.jackson.Serialization.{read, write, writePretty}
import org.json4s.{DefaultFormats, FieldSerializer, Formats}

import scala.reflect.Manifest

object JsonUtils {

  private val throwableSerializer = FieldSerializer[Throwable](ignore("stackTrace") orElse ignore("cause"))

  implicit lazy val formats = DefaultFormats.withBigDecimal + throwableSerializer

  def toJson[A <: AnyRef](a: A, pretty: Boolean = false)(implicit formats: Formats): String = {
    if (pretty) {
      writePretty(a)
    } else {
      write(a)
    }
  }

  def fromJson[A](json: String)(implicit formats: Formats, mf: Manifest[A]): A = {
    read[A](json)
  }

}
