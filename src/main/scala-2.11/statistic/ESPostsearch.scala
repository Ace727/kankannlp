package statistic

import java.io.PrintWriter
import java.text.SimpleDateFormat
import java.util.Date
import java.util.concurrent.TimeUnit

import com.sksamuel.elastic4s.ElasticDsl.{field, search, _}
import com.sksamuel.elastic4s.{ElasticClient, ElasticsearchClientUri, QueryDefinition}
import com.typesafe.config.ConfigFactory
import common.HttpUtils
import org.elasticsearch.common.settings.Settings
import org.slf4j.{Logger, LoggerFactory}
import JsonUtils.formats

import scala.collection.mutable.{ArrayBuffer, ListBuffer}

/**
  * Created by remarkmedia on 17/2/24.
  */
object ESPostsearch {
  private val config = ConfigFactory.load("application.conf")
  val dateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-M-d H:m:s")
  val date1start : Date = dateFormat.parse("2017-2-11 19:00:00")
  val date1end : Date = dateFormat.parse("2017-2-11 22:00:00")
  val date2start : Date = dateFormat.parse("2017-2-12 19:00:00")
  val date2end : Date = dateFormat.parse("2017-2-12 22:00:00")
  val likesCountLow = 5
  val likeCountMid = 100
  val likeCountHigh = 500
  val batchsize = 20
  val searchedPost : ArrayBuffer[String] = new ArrayBuffer[String]()
  val logger : Logger = ESPostsearch.logger
  private val settings = Settings.settingsBuilder().put("cluster.name", config.getString("elasticsearch.id")).build()

  private val client = ElasticClient.transport(settings, ElasticsearchClientUri(config.getString("elasticsearch.uri")))





  /**
    *
    * @param searchInfo Search Information
    */
  def searchPostBot(searchInfo: SearchInfo) ={
    val totalQueryList : ListBuffer[QueryDefinition] = new ListBuffer[QueryDefinition]
    val targetQuery = termQuery("target", searchInfo.target)
    totalQueryList.append(targetQuery)

    if(searchInfo.startTime.nonEmpty && searchInfo.endTime.nonEmpty){
      val timeQuery = rangeQuery("timestamp").gte(searchInfo.startTime.get.getTime).lte(searchInfo.endTime.get.getTime)
      totalQueryList.append(timeQuery)
    }

    if( searchInfo.shouldKeyWords.nonEmpty){
      val shouldkwQueries : ListBuffer[QueryDefinition] = new ListBuffer[QueryDefinition]
      searchInfo.shouldKeyWords.get.foreach{ kword =>
        val kwQuery = termQuery("content", kword)
        shouldkwQueries.append(kwQuery)
      }
      totalQueryList.append(should(shouldkwQueries))
    }

    if( searchInfo.mustKeyWords.nonEmpty){
      val mustkwQeries : ListBuffer[QueryDefinition] = new ListBuffer[QueryDefinition]
      if (searchInfo.mustKeyWords.get.size >= 3){
        logger.warn("mustKeyWords size is more than 2 words.")
      }
      searchInfo.mustKeyWords.get.foreach{ kword =>
        val kwQuery = termQuery("content",kword)
        mustkwQeries.append(kwQuery)
      }
      totalQueryList.append(must(mustkwQeries))
    }

    if(searchInfo.targetNames.nonEmpty){
      val targetNameQueries : ListBuffer[QueryDefinition] = new ListBuffer[QueryDefinition]
      searchInfo.targetNames.get.foreach{ targetname=>
        val tnQuery = termQuery("targetId", targetname)
        targetNameQueries.append(tnQuery)
      }
      totalQueryList.append(should(targetNameQueries))
    }

    val likeQuery = rangeQuery("likesCount").gte(searchInfo.likesCount)
    val commentQuery = rangeQuery("commentsCount").gte(searchInfo.commentsCount)
    totalQueryList.append(likeQuery)
    totalQueryList.append(commentQuery)
    val totalQuery = {bool(must(totalQueryList))}
    println(totalQuery.toString)
    val resp = elasticsearch.scrollQuery(client,"post_*","p",totalQuery)
    resp
  }


  def searchPostEXO () ={
    val query = {
      bool(
        must(
          termQuery("target", "instagram"),
          //          geoDistanceQuery("geo").point(lationLat,lationLon).distance(5000,DistanceUnit.METERS),
          should(
            rangeQuery("timestamp").gte(date1start.getTime).lte(date1end.getTime),
            rangeQuery("timestamp").gte(date2start.getTime).lte(date2end.getTime)
          ),
          should(
            termQuery("content", "exordiuminhk"),
            termQuery("content", "4everchanbaek"),
            must(
              termQuery("content", "exo"),
              termQuery("content", "hongkong")
            ),
            //            termQuery("content", "exo"),
            termQuery("content", " #EXOrDIUMinHongKong")

          ) minimumShouldMatch 1,
          rangeQuery("commentsCount").gte(0),
          rangeQuery("likesCount").gte(5)

        )
      )
    }

    val resp = elasticsearch.scrollQuery(client,"post_*","p",query)
    resp
  }

  def getPlatformId( target: String):String ={
    target match {
      case "unknown" => "-1"
      case "weibo" => "1"
      case "weixin" => "2"
      case "renren" => "3"
      case "instagram" => "4"
      case "qqzone" => "5"
      case "cicada_travel" => "6"
      case "yelp" =>  "7"
      case "dianping" => "8"
      case "facebook" => "9"
      case "pinterest" => "10"
      case "twitter" => "11"
      case "tumblr" => "12"
      case "snapchat" => "13"
      case "foursquare" => "14"
      case "groupon" => "15"
      case "livingsocial" => "16"
      case "kakao" => "17"
      case "kankan" => "18"
      case "youtube" => "19"
      case _ => "0"
    }
  }

  def searchResult2Json(jsonFile:String, searchInfo: SearchInfo, postUrl: String): Unit ={
    val result = searchPostBot(searchInfo)
    val hitsIter = result.iterator
    var trandArray = new ArrayBuffer[TrendingDetailInfo]()
    var count = 0
    while(hitsIter.hasNext){
      val hit = hitsIter.next()
      val post_id = hit.id()
      val sourceMap = hit.getSource
      //获取profileInfo
      val targetId = sourceMap.get("targetId").toString
      val nickname = sourceMap.get("targetName").toString
      val avatar = sourceMap.get("targetAvatar").toString
      val target = sourceMap.get("target").toString
      val profileInfo = new ProfileInfo(nickname, avatar, targetId, getPlatformId(searchInfo.target))
      val postType = sourceMap.get("type").toString

      val postid = sourceMap.get("id").toString
      val timestamp = sourceMap.get("timestamp").toString.toLong
      val content = sourceMap.get("content").toString
      val likescount = sourceMap.get("likesCount").toString.toInt
      val commentscount = sourceMap.get("commentsCount").toString.toInt
      var imgurl : Option[String] = None
      var videoinfo : Option[VideoDetailInfo] = None
      //postType 等于image
      if(postType.equals("image") && likescount > searchInfo.imageLevel){
        val imageurl = HbasePostSearch.searchImageUrl(targetId,target,post_id)
        imgurl  = Some(imageurl)
        val trendingDetailInfo = new TrendingDetailInfo(searchInfo.subCategoryId, postid, timestamp, profileInfo, postType, content, imgurl, videoinfo,likeCount = likescount,commentCount = commentscount)
        trandArray += trendingDetailInfo
        count += 1
      }else if(postType.equals("video") && likescount > searchInfo.videoLevel) {
        //postType 等于video 获取videoDetailInfo
        val videoDetailInfo = HbasePostSearch.searchVideoUrl(targetId, target, post_id)
        if (!videoDetailInfo.videoUrl.equals("") && !videoDetailInfo.coverUrl.equals("")) {
          videoinfo = Some(videoDetailInfo)
          val trendingDetailInfo = new TrendingDetailInfo(searchInfo.subCategoryId, postid, timestamp, profileInfo, postType, content, imgurl, videoinfo, likeCount = likescount, commentCount = commentscount)
          trandArray += trendingDetailInfo
          count += 1
        }
      }

      if (count % batchsize == 0){
        val pw_json = new PrintWriter(jsonFile)
        val trandList = trandArray.toList
        val batchTrendingDetailMessage = new BatchTrendingDetailMessage(trandList)
        val jsonstr = JsonUtils.toJson(batchTrendingDetailMessage, false)
        pw_json.write(jsonstr+"\n")
        pw_json.close()
        trandArray = new ArrayBuffer[TrendingDetailInfo]()
        HttpUtils.httpPost(jsonstr, postUrl)
        println(batchsize+" files sent to API")
        TimeUnit.MINUTES.sleep( 5 )
      }
    }
    val pw_json = new PrintWriter(jsonFile)
    val trandList = trandArray.toList
    val batchTrendingDetailMessage = new BatchTrendingDetailMessage(trandList)
    val jsonstr = JsonUtils.toJson(batchTrendingDetailMessage, false)
    pw_json.write(jsonstr+"\n")
    pw_json.close()
    HttpUtils.httpPost(jsonstr, postUrl)
    println("sent files finish.")
  }


  def followTopic(jsonFile: String, searchInfo: SearchInfo, postUrl:String, followTime: Long): Unit ={
    var quit = false
    while(!quit){
      val now : Date = new Date()
      if( now.getTime < followTime){
        searchResult2Json(jsonFile, searchInfo, postUrl)
        TimeUnit.HOURS.sleep(4)
      }else{
        quit = true
      }
    }
  }

  def main(args:Array[String]): Unit= {
    val jsonFile = "trends.json"
    val searchInfo : SearchInfo = new SearchInfo()
    searchInfo.subCategoryId = 3547l
    searchInfo.target = "instagram"
    val dateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-M-d H:m:s")
    val date1start : Date = dateFormat.parse("2017-2-25 19:30:00")
    val date1end : Date = dateFormat.parse("2017-2-26 22:00:00")
    val shouldKeyWords = List("exordiuminmanila")
    searchInfo.shouldKeyWords = Option(shouldKeyWords)
    searchInfo.startTime = Option(date1start)
    searchInfo.endTime = Option(date1end)
    searchInfo.likesCount = 5
    searchInfo.commentsCount = 0
    searchInfo.mustKeyWords = None
    searchInfo.targetNames = None
    val followTime = new Date().getTime+36000
    val postUrl = "http://sdk.stage.pub.hzvb.kankanapp.com.cn/api/V2/trendingDetail/batch"
  }
}
