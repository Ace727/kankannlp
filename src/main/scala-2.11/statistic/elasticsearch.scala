package statistic

import java.io.PrintWriter
import java.text.SimpleDateFormat
import java.util.Date

import com.sksamuel.elastic4s.{ElasticClient, ElasticsearchClientUri, QueryDefinition, RangeQueryDefinition}
import com.typesafe.config.ConfigFactory
import org.elasticsearch.action.admin.indices.stats.IndicesStatsRequest
import org.elasticsearch.common.settings.Settings
import com.sksamuel.elastic4s.ElasticDsl._
import org.elasticsearch.common.unit.DistanceUnit
import org.elasticsearch.search.SearchHit
import org.elasticsearch.search.sort.SortOrder

import scala.collection.JavaConversions._
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.concurrent.duration._
import JsonUtils.formats
import common.HttpUtils
import java.util.concurrent.TimeUnit

import scala.collection.mutable

/**
  * Created by dengsi on 2017/2/7.
  */
object elasticsearch {
  private val config = ConfigFactory.load("application.conf")
  val dateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-M-d H:m:s")
  val date1start : Date = dateFormat.parse("2017-2-11 19:00:00")
  val date1end : Date = dateFormat.parse("2017-2-11 22:00:00")
  val date2start : Date = dateFormat.parse("2017-2-12 19:00:00")
  val date2end : Date = dateFormat.parse("2017-2-12 22:00:00")

  private val settings = Settings.settingsBuilder().put("cluster.name", config.getString("elasticsearch.id")).build()

  private val client = ElasticClient.transport(settings, ElasticsearchClientUri(config.getString("elasticsearch.uri")))

  def searchProfile(target: String, targetId: String, name: String, identity: Int) = {
    val query = {
      val tmpQuery = {
        if (targetId == "" && name == "") {
          bool(
            must {
              termQuery("target", target)
            }
          )
        } else {
          bool(
            should(
              termQuery("targetId", targetId),
              prefixQuery("keywords", name),
              prefixQuery("name", name)
            ) minimumShouldMatch 1
              must termQuery("target", target)
          )
        }
      }

      if (identity == 1) {
        tmpQuery.must(termQuery("identity", 1))
      } else if (identity == -1) {
        tmpQuery
      } else {
        tmpQuery.not(termQuery("identity", 1))
      }
    }

    val resp = client.execute {
      search in "profile" query {
        query
      } sort (field sort "fansCount" order SortOrder.DESC)
    }.await

    resp.getHits
  }

  def searchProfileByTarget(target: String) = {
    val resp = client.execute {
      search in "profile" query {
        bool(
          should(
            termQuery("target", target)
          )
        )
      }
    }.await

    resp.getHits
  }

  def searchStore() = {
    val resp = client.execute {
      search in "store"
    }.await

    resp.getHits
  }

  def searchPost(target: String, date: String) = {
    val resp = client.execute {
      search in s"post_${date}" query {
        bool(
          should(
            termQuery("target", target)
          )
        )
      }
    }.await

    resp.getHits
  }

  def searchPostIndexes(limit: Int = Int.MaxValue) = {
    val resp = client.admin.indices().stats(new IndicesStatsRequest()).get()
    resp.getIndices.keySet().filter(_.startsWith("post_")).filter(_ <= "post_" + getCurMonth).toList.sortWith((x, y) => x > y).slice(0, limit)
  }

  def getCurMonth = {
    val sdf = new SimpleDateFormat("yyyy_MM")
    sdf.format(new Date())
  }

  /**
    * 滚动查询es所有满足条件的数据
    *
    * @param client
    * @param index
    * @param tp
    * @param query
    * @return
    */
  def scrollQuery(client: ElasticClient, index: String, tp: String, query: => QueryDefinition) = {
    val hitList = new ListBuffer[SearchHit]

    var resp = client.execute {
      search(index / tp).query(query) scroll "5m" size 10 sort (field sort "likesCount" order SortOrder.DESC)
    }.await
    hitList ++= resp.getHits.getHits

    while (!resp.isEmpty) {
      resp = client.execute {
        searchScroll(resp.getScrollId).keepAlive("5m")
      }.await
      hitList ++= resp.getHits.getHits
    }

    hitList.toList
  }


  /**
    *
    * @param target post target
    * @param startTime startTime of search, option co-present with endTime
    * @param endTime endTime of search, option co-present with startTime
    * @param shouldKeyWords  words of should query
    * @param mustKeyWords words of should query , less than 3 words , option
    * @param likesCount likesCount of post
    * @param commentsCount commentsCount of post
    */
  def searchPostBot(target: String, startTime: Option[Date], endTime: Option[Date], shouldKeyWords: List[String],
                    mustKeyWords: Option[List[String]], likesCount: Int, commentsCount: Int): Unit ={
    val totalList : ListBuffer[QueryDefinition] = new ListBuffer[QueryDefinition]
    val targetQuery = termQuery("target", target)
    if(startTime != None && endTime != None){
      val timeQuery = rangeQuery("timestamp")
    }

  }

  def searchPostEXO () ={
    val query = {
      bool(
        must(
          termQuery("target", "instagram"),
//          geoDistanceQuery("geo").point(lationLat,lationLon).distance(5000,DistanceUnit.METERS),
          should(
            rangeQuery("timestamp").gte(date1start.getTime).lte(date1end.getTime),
              rangeQuery("timestamp").gte(date2start.getTime).lte(date2end.getTime)
          ),
          should(
            termQuery("content", "exordiuminhk"),
            termQuery("content", "4everchanbaek"),
            must(
              termQuery("content", "exo"),
              termQuery("content", "hongkong")
            ),
//            termQuery("content", "exo"),
            termQuery("content", " #EXOrDIUMinHongKong")

          ) minimumShouldMatch 1,
          rangeQuery("commentsCount").gte(0),
          rangeQuery("likesCount").gte(5)

        )
      )
    }

    val resp = scrollQuery(client,"post_*","p",query)
    resp
  }

  def searchProfilefans(targetId: String, target: String)={
    val query = {
      must(
        termQuery("targetId", targetId),
        termQuery("target", target)
      )
    }
    val resp = client.execute {
            search in "profile" query {
              query
            } sort (field sort "fansCount" order SortOrder.DESC)
          }.await(1 day)
    val hitsIter = resp.getHits.getHits.iterator
    var fans = 0
    while(hitsIter.hasNext){
      val sourceMap = hitsIter.next().sourceAsMap()
//      sourceMap.foreach( p => print(p._1+":"+p._2+"\n"))
      fans = Integer.parseInt(sourceMap.get("fansCount").toString)
//      print("fansCount is:"+ fans+"\n")
    }
    fans
  }

  def searchResult2Json(jsonFile:String): Unit ={

    val result = searchPostEXO()
    val hitsIter = result.iterator
    var trandArray = new ArrayBuffer[TrendingDetailInfo]()
    var count = 0
    val patchUrl = "http://sdk.stage.pub.hzvb.kankanapp.com.cn/api/V2/trendingDetail/batch"

    while(hitsIter.hasNext){
      val hit = hitsIter.next()
      val post_id = hit.id()
      val sourceMap = hit.getSource
      //获取profileInfo
      val nickname = sourceMap.get("targetName").toString
      val avatar = sourceMap.get("targetAvatar").toString
      val targetId = sourceMap.get("targetId").toString
      val target = sourceMap.get("target").toString
      val profileInfo = new ProfileInfo(nickname, avatar, targetId,"4")
      val postType = sourceMap.get("type").toString
      val subCategoryId = 3512l
      val postid = sourceMap.get("id").toString
      val timestamp = sourceMap.get("timestamp").toString.toLong
      val content = sourceMap.get("content").toString
      val likescount = sourceMap.get("likesCount").toString.toInt
      val commentscount = sourceMap.get("commentsCount").toString.toInt
      var imgurl : Option[String] = None
      var videoinfo : Option[VideoDetailInfo] = None
      //postType 等于image
      if(postType.equals("image")){
        val imageurl = HbasePostSearch.searchImageUrl(targetId,target,post_id)
        imgurl  = Some(imageurl)
        val trendingDetailInfo = new TrendingDetailInfo(subCategoryId,postid, timestamp, profileInfo, postType, content, imgurl, videoinfo,likeCount = likescount,commentCount = commentscount)
        trandArray += trendingDetailInfo
        count += 1
      }else if(postType.equals("video")) {
        //postType 等于video 获取videoDetailInfo
        val videoDetailInfo = HbasePostSearch.searchVideoUrl(targetId, target, post_id)
        if (!videoDetailInfo.videoUrl.equals("") && !videoDetailInfo.coverUrl.equals("")) {
          videoinfo = Some(videoDetailInfo)
          val trendingDetailInfo = new TrendingDetailInfo(subCategoryId, postid, timestamp, profileInfo, postType, content, imgurl, videoinfo, likeCount = likescount, commentCount = commentscount)
          trandArray += trendingDetailInfo
          count += 1
        }
      }

      if (count % 20 == 0){
        val pw_json = new PrintWriter(jsonFile)
        val trandList = trandArray.toList
        val batchTrendingDetailMessage = new BatchTrendingDetailMessage(trandList)
        val jsonstr = JsonUtils.toJson(batchTrendingDetailMessage, false)
        pw_json.write(jsonstr+"\n")
        pw_json.close()
        trandArray = new ArrayBuffer[TrendingDetailInfo]()
        HttpUtils.httpPost(jsonstr, patchUrl)
        println("20 files sent to API")
        TimeUnit.MINUTES.sleep(5)
      }
    }
    val pw_json = new PrintWriter(jsonFile)
    val trandList = trandArray.toList
    val batchTrendingDetailMessage = new BatchTrendingDetailMessage(trandList)
    val jsonstr = JsonUtils.toJson(batchTrendingDetailMessage, false)
    pw_json.write(jsonstr+"\n")
    pw_json.close()
    HttpUtils.httpPost(jsonstr, patchUrl)
    println("sent files finish.")
  }


  def main(args:Array[String]): Unit= {
    val result = searchPostEXO()
    //    val totalhits = result.totalHits()
    //    print("total hits ist:"+totalhits+"\n")
    //    val hits = result.hits()
        val hitsIter = result.iterator
        val pw_video = new PrintWriter("tagMerge_video.txt")
        val pw_image = new PrintWriter("tagMerge_image.txt")
        while(hitsIter.hasNext){
          val hit = hitsIter.next()
          val post_id = hit.id()
          val sourceMap = hit.getSource
          val timestamp = sourceMap.get("timestamp")
          val target = sourceMap.get("target").toString
          val targetId = sourceMap.get("targetId").toString
          val likeCount = sourceMap.get("likesCount")
          val commentCount = sourceMap.get("commentsCount")
          val content = sourceMap.get("content").toString.replace("\n","")
          val postType = sourceMap.get("type").toString
          var profilefans = searchProfilefans(targetId.toString, target.toString)
          if (postType.equals("image")){
    //        val hiturl = "http://prod-api.kankanapp.com.cn/api/v3/post?profileId="+targetId+":"+target+"&timestamp="+timestamp        print(postType+"\t"+post_id+"\t"+targetId+"\t"+commentCount+"\t" +profilefans+ "\n")
            var url = HbasePostSearch.searchPostURL(targetId,target,post_id,postType)
            pw_image.write(postType+"\t"+url+"\t"+likeCount+"\t"+commentCount+"\t" +profilefans+ "\n")
          }

          if(postType.equals("video")){
            var url = HbasePostSearch.searchPostURL(targetId,target,post_id,postType)
            pw_video.write(postType+"\t"+url+"\t"+likeCount+"\t"+commentCount+"\t" +profilefans+ "\n")
          }


        }
        pw_video.close()
        pw_image.close()


//    val jsonFile = "trends.json"
//    searchResult2Json(jsonFile)


  }
}