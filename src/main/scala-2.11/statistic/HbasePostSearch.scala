package statistic

import java.nio.ByteBuffer
import java.util.Base64

//import common.ExportProfileContent._
import common.HbaseColumn
import common.HbaseColumn.video_table
import org.apache.commons.codec.digest.DigestUtils
import org.apache.hadoop.hbase.{HBaseConfiguration, TableName}
import org.apache.hadoop.hbase.client.{Connection, ConnectionFactory, Get, Scan}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.commons.lang.StringUtils._
import JsonUtils.formats




/**
  * Created by remarkmedia on 17/2/17.
  */
object HbasePostSearch extends HbaseColumn.video_table with HbaseColumn.image_table with HbaseColumn.Post_mate{
  val imageTableName = "image"
  val videoTableName = "video"
  val batchSize = 1000

  def createNewConnection(): Connection = {
    val configuration = HBaseConfiguration.create()
    configuration.set("hbase.zookeeper.property.clientPort", "2181")
    configuration.set("hbase.zookeeper.quorum", "phb03")
    configuration.setLong("hbase.rpc.timeout", 24 * 60 * 60 * 1000)
    val conn = ConnectionFactory.createConnection(configuration)
    conn
  }

  def profileESId(profileHbaseRowKey: Array[Byte]): String = {
    Base64.getEncoder.encodeToString(profileHbaseRowKey)
  }

  def postHbaseRowkey(targetId: String, target: String, postEsId: String) ={
    val profileHbaseRowKey = DigestUtils.md5(targetId + ":" + target)
    val profileEsId = profileESId(profileHbaseRowKey)
//    print(profileEsId+"\n")
    val postEsIdHex = Bytes.toStringBinary(Base64.getDecoder.decode(postEsId))
    val profileIdHex = Bytes.toStringBinary(Base64.getDecoder.decode(profileEsId))
    val postHbaseRowKey = profileIdHex + replaceOnce(postEsIdHex, profileIdHex, EMPTY)
    val postHbaseRowKey_byte = Bytes.toBytesBinary(postHbaseRowKey)
    postHbaseRowKey_byte
  }

  def postImageRowKey(postRowKey: Array[Byte], sequence: Int) = {
    val bbf = ByteBuffer.allocate(33)
    bbf.put(postRowKey)
    bbf.put(sequence.toByte)
    bbf.array()
  }

  def searchPostMeta(targetId_str: String, target_str: String, postEsId: String, postType: String):Unit={
    val conn = createNewConnection()
    var tableName = "post_meta"
    val postRowkey = postHbaseRowkey(targetId_str, target_str, postEsId)
//    print ("postRowkey="+postRowkey+"\n")
    val table = conn.getTable(TableName.valueOf(tableName))
    val result = table.get(new Get(postRowkey))
    if(result.isEmpty){
      print(" result is empty!")
    }
    val scan = new Scan()
    scan.setBatch(batchSize)
    scan.setCacheBlocks(false)
    scan.setStartRow(postRowkey)
    scan.setStopRow(postRowkey ++ HbaseColumn.STOP)
    val post_scanner = table.getScanner(scan)
    val iterator = post_scanner.iterator()

    while (iterator.hasNext) {
      val result = iterator.next()
      var content = ""
      var target = ""
      print("iterator is not null.")
      try {
        val targetId = new String(result.getValue(HbaseColumn.CF, POST_META_TARGET_ID))
        val content = new String(result.getValue(HbaseColumn.CF, POST_META_CONTENT))
        print("targetId is:" + targetId + "\t" + "content is:" + content + "\n")
      } catch {
        case npe: NullPointerException => {
          //          println(npe.getMessage)
        }
      }
    }
    conn.close()

  }


  def searchPostURL(targetId_str: String, target_str: String, postEsId: String, postType: String):String ={
    val conn = createNewConnection()
    var tableName = ""
    val postRowkey = postHbaseRowkey(targetId_str, target_str, postEsId)
//    println(Bytes.toStringBinary(postRowkey))
    var url = ""
    var subRowkey:Array[Byte] = null
    if( postType.equals(imageTableName)) {
      tableName = imageTableName

      subRowkey = postImageRowKey(postRowkey,2)
    }
    else if( postType.equals(videoTableName)) {
      tableName = videoTableName

      subRowkey = postRowkey
    }
    else {
      print("error: type is not image or video.")
      conn.close()
      return url
    }
//    println("tablename is:"+tableName)
    val table = conn.getTable(TableName.valueOf(tableName))
    val result = table.get(new Get(subRowkey))
    if(result.isEmpty){
      print ("result is empty. target id is:"+targetId_str+"\n")
      conn.close()
      return  url
    }else{
      if(postType.equals(videoTableName)){
        try {
          val ossurl = new String(result.getValue(HbaseColumn.CF, OSSURL))
          url = ossurl
        }catch {
          case npe: NullPointerException => {
            //          println(npe.getMessage)
            url = new String(result.getValue(HbaseColumn.CF, voriginalUrl))
          }
        }
      }else if(postType.equals(imageTableName)){
        try {
          val originURL = new String(result.getValue(HbaseColumn.CF, originalUrl))
          val imgType = new String(result.getValue(HbaseColumn.CF, imageType))
//          println("imageType is:"+imgType)
          url = originURL
        }catch {
          case npe: NullPointerException =>{
            println(npe.getMessage)
          }
        }
      }

      conn.close()
      return url
    }
  }

  def searchImageUrl(targetId_str: String, target_str: String, postEsId: String): String ={
    val conn = createNewConnection()
    val postRowkey = postHbaseRowkey(targetId_str, target_str, postEsId)
//    println(Bytes.toStringBinary(postRowkey))
    var url = ""
    var subRowkey:Array[Byte] = postImageRowKey(postRowkey,2)
    val table = conn.getTable(TableName.valueOf(imageTableName))
    val result = table.get(new Get(subRowkey))
    if(result.isEmpty){
      print ("result is empty. target id is:"+targetId_str+"\n")
      conn.close()
      return  url
    }else{
        try {
          val originURL = new String(result.getValue(HbaseColumn.CF, originalUrl))
          val imgType = new String(result.getValue(HbaseColumn.CF, imageType))
//          println("imageType is:"+imgType)
          url = originURL
        }catch {
          case npe: NullPointerException =>{
            println(npe.getMessage)
          }
        }
      conn.close()
      return url
    }
  }

  def searchVideoUrl(targetId_str: String, target_str: String, postEsId: String):VideoDetailInfo={
    val conn = createNewConnection()
    var tableName = "video"
    val postRowkey = postHbaseRowkey(targetId_str, target_str, postEsId)
//    println(Bytes.toStringBinary(postRowkey))
    var oriurl = ""
    var coverurl = ""
    val subRowkey:Array[Byte] = postRowkey
    val table = conn.getTable(TableName.valueOf(tableName))
    val result = table.get(new Get(subRowkey))
    if(result.isEmpty){
      print ("result is empty. target id is:"+targetId_str+"\n")
      conn.close()
      return  new VideoDetailInfo("","")
    }else{
        try {
          val ossurl = new String(result.getValue(HbaseColumn.CF, OSSURL))
          val coverUrl = new String(result.getValue(HbaseColumn.CF, voriginalCoverUrl))
          oriurl = ossurl
          coverurl = coverUrl
        }catch {
          case npe: NullPointerException => {
            //          println(npe.getMessage)
            oriurl = new String(result.getValue(HbaseColumn.CF, voriginalUrl))
            coverurl = new String(result.getValue(HbaseColumn.CF, voriginalCoverUrl))
//            println("ossurl is:"+oriurl+"\ncoverurl is:"+coverurl)
          }
        }
      conn.close()
      var videoDetailInfo = new VideoDetailInfo(coverurl,oriurl)
      return videoDetailInfo
    }
  }



  def main(args:Array[String]): Unit= {
    val postType = "video"
    val targetId = "3402215262"
    val target = "instagram"
    val postid = "f//+pdL9h7ePlLpQk+uFL4mMcy43JTLWupaDwCbSBwA="
    val videoinfo = searchVideoUrl(targetId, target, postid)
    val jsonstr = JsonUtils.toJson(videoinfo, false)
    println("jsonstr is:"+jsonstr)
    val timestamp = 1486815260000L

//    val hiturl = "http://prod-api.kankanapp.com.cn/api/v3/post?profileId="+targetId+":"+target+"&timestamp="+timestamp
//    val reponse = HttpUtils.httpGet(hiturl)
//    println("reponse is: "+reponse.toString)
//    val jsonObj = JsonParser(reponse).asJsObject()
//
//    val content = jsonObj.getFields("content").toJson.asJsObject()
//    val imageUrl = content.getFields("images").toJson.asJsObject()
//
//
//    println ("imageurl is:"+imageUrl.toString())


  }

}
