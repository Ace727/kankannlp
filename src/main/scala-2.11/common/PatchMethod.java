package common;

import org.apache.commons.httpclient.methods.EntityEnclosingMethod;


/**
 * Created by remarkmedia on 17/2/22.
 */
public class PatchMethod extends EntityEnclosingMethod {
    public PatchMethod() {
        super();
    }

    public PatchMethod(String url) {
        super(url);
    }
    @Override
    public String getName() {
        return "PUT";
    }
}
