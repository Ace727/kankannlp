package common

import org.apache.commons.codec.digest.DigestUtils
import org.apache.hadoop.hbase._
import org.apache.hadoop.hbase.client._

/**
  * Created by remarkmedia on 16/8/5.
  */
object Hbase {
  //val conn=Connection
  def createNewConnection(): Connection = {
    val configuration = HBaseConfiguration.create()
    configuration.set("hbase.zookeeper.property.clientPort", "2181")
    configuration.set("hbase.zookeeper.quorum", "phb01,phb02,phb03")
    configuration.setLong("hbase.rpc.timeout", 24 * 60 * 60 * 1000)
    val conn = ConnectionFactory.createConnection(configuration)
    conn
  }


  def profileRowKey(targetId: String, target: String) = {
    DigestUtils.md5(targetId + ":" + target)
  }

  def profileFriendRowKey(targetId: String, target: String, friendId: String): Array[Byte] = {
    profileRowKey(targetId, target) ++ (friendId + ":" + target).getBytes()
  }

//  def main(): Unit ={
//    val conn = createNewConnection()
//    val tb = conn.getTable(TableName.valueOf("profile"))
//    val friend = conn.getTable(TableName.valueOf("profile_friend"))
//    val scan = new Scan()
//    scan.setBatch(10000)
////    val scanner = new
//  }

//  def main1(args: Array[String]) {
//    if (args.length < 1) {
//      System.err.println("Usage: <table_name>")
//      System.exit(1)
//    }
//    val f = Array[Byte](0x41.toByte)
//    val targetId = Array[Byte](0x00.toByte)
//    val target = Array[Byte](0x01.toByte)
//    val gender = Array[Byte](0x12.toByte)
////    info("begin count table,table name:" + args(0))
//
//    val sparkConf = new SparkConf().setAppName("HtableCount:" + args(0)).setMaster("yarn-cluster")
//    val sc = new SparkContext(sparkConf)
//
//    val scan = new Scan()
//    scan.setCacheBlocks(false)
//
//    scan.setBatch(10000)
//    val proto = ProtobufUtil.toScan(scan)
//    val ScanToString = Base64.encodeBytes(proto.toByteArray())
//    val conf = HBaseConfiguration.create()
//
//    conf.set(TableInputFormat.INPUT_TABLE, args(0))
//    conf.set(TableInputFormat.SCAN, ScanToString)
//    conf.set("hbase.zookeeper.property.clientPort", "2181")
//    conf.set("hbase.zookeeper.quorum", "phb01,phb02,phb02")
//    conf.setLong("hbase.rpc.timeout", 24 * 60 * 60 * 1000)
//
//    val hBaseRDD = sc.newAPIHadoopRDD(conf, classOf[TableInputFormat],
//      classOf[org.apache.hadoop.hbase.io.ImmutableBytesWritable],
//      classOf[org.apache.hadoop.hbase.client.Result])
//
//    val tableCount = hBaseRDD.count()
//    printf(args(0) + "count:" + tableCount)
////    info(args(0) + "count:" + tableCount)
//    sc.stop()
//    hBaseRDD.foreachPartition(
//      partition => {
//        if (partition != null && !partition.isEmpty) {
//          val conn = Hbase.createNewConnection()
//          val table = conn.getTable(TableName.valueOf("weibo_female"))
//          partition.foreach(
//            data => {
//              val gender_val = Bytes.toInt(data._2.getValue(f, gender))
//              val targetId_val = new String(data._2.getValue(f, targetId))
//              val target_val = new String(data._2.getValue(f, target))
//              target_val match {
//                case "weibo" => {
//                  gender_val match {
//                    case 2 => {
//                      table.put(new Put(targetId_val.getBytes()).addColumn("info".getBytes(), "gender".getBytes(), Bytes.toBytes(gender_val)))
//                    }
//                    case _ =>
//                  }
//                }
//
//                case _ =>
//              }
//
//            }
//          )
//        }
//
//      }
//    )
//  }


}


