name := "kankanNLP"

version := "1.0"

scalaVersion := "2.11.7"


resolvers in ThisBuild += "cloudera" at "https://repository.cloudera.com/artifactory/cloudera-repos/"

resolvers in ThisBuild += "mvnrepository" at "http://mvnrepository.com/"

libraryDependencies ++= dependencies.json

libraryDependencies ++= {
  val hbaseV = "1.0.0-cdh5.5.1"
  Seq(
    ("org.apache.hbase" % "hbase-common" % hbaseV).
      exclude("com.typesafe.play", "sbt-link").exclude("org.mortbay.jetty", "servlet-api-2.5").
      exclude("org.mortbay.jetty", "jetty-util").
      exclude("org.mortbay.jetty", "jetty").exclude("io.netty","netty").
      exclude("tomcat", "jasper-compiler").exclude("org.mortbay.jetty","jsp-2.1").exclude("tomcat","jasper-runtime"),
    ("org.apache.hbase" % "hbase-client" % hbaseV).exclude("commons-logging", "commons-logging").
      exclude("com.typesafe.play", "sbt-link").exclude("org.mortbay.jetty", "servlet-api-2.5").
      exclude("org.mortbay.jetty", "jetty-util").exclude("io.netty","netty").
      exclude("org.mortbay.jetty", "jetty").
      exclude("tomcat", "jasper-compiler").exclude("org.mortbay.jetty","jsp-2.1").exclude("tomcat","jasper-runtime"),
    ("org.apache.hbase" % "hbase-server" % hbaseV).exclude("commons-logging", "commons-logging").
      exclude("com.typesafe.play", "sbt-link").exclude("org.mortbay.jetty", "servlet-api-2.5").
      exclude("org.mortbay.jetty", "jetty-util").
      exclude("org.mortbay.jetty", "jetty").
      exclude("tomcat", "jasper-compiler").exclude("org.mortbay.jetty","jsp-2.1").exclude("tomcat","jasper-runtime")
        .exclude("io.netty","netty"),
    ("org.apache.hadoop" % "hadoop-common" % "2.6.0-cdh5.5.1" % "provided").exclude("io.netty","netty"),
    "com.sksamuel.elastic4s" %% "elastic4s-core" % "2.1.2-shade" % "provided",
    "com.sksamuel.elastic4s" %% "elastic4s-jackson" % "2.1.2-shade" % "provided",
    "com.sksamuel.elastic4s" %% "elastic4s-json4s" % "2.1.2-shade" % "provided",
    "com.typesafe" % "config" % "1.3.1",
    "com.squareup.okhttp3" % "okhttp" % "3.6.0"

//    "com.typesafe.play" %% "play-ws" % "2.4.3"
  )


}